<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Badge OSMIUM</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.2/html2canvas.min.js"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap');
        body {
            font-family: 'Poppins', sans-serif;
        }
    </style>
</head>
<body class="bg-gray-100 flex items-center justify-center min-h-screen p-4">
<div class="container max-w-md mx-auto">
    <div id="badge" class="w-80 h-[500px] bg-white rounded-2xl shadow-2xl overflow-hidden relative">
        <!-- Header -->
        <div class="h-32 bg-gradient-to-r from-yellow-400 via-yellow-300 to-yellow-200 relative">
            <div class="absolute top-4 left-4 flex items-center">
                <div class="w-20 h-20 bg-white rounded-full flex items-center justify-center mr-3 border-2 border-green-700 shadow-lg">
                    <img src="https://www.claudeusercontent.com/api/placeholder/72/72" alt="Logo" class="object-cover h-16 w-16 rounded-full">
                </div>
                <div class="text-green-800">
                    <h1 class="text-2xl font-bold leading-tight">OSMIUM</h1>
                    <p class="text-sm font-semibold">FOOTBALL CLUB YAOUNDÉ II</p>
                </div>
            </div>
        </div>

        <!-- Main Content -->
        <div class="p-6">
            <div class="flex justify-center -mt-16">
                <div class="w-36 h-36 rounded-full overflow-hidden border-4 border-white shadow-xl">
                    <img src="https://www.claudeusercontent.com/api/placeholder/144/144" alt="Member" class="w-full h-full object-cover">
                </div>
            </div>

            <div class="text-center mt-4">
                <h2 class="text-xl font-bold text-green-800">NGONO Christelle</h2>
                <p class="text-sm text-green-700 font-semibold mt-1">CARTE DE MEMBRE</p>
            </div>

            <div class="mt-6 flex justify-center">
                <img src="https://www.claudeusercontent.com/api/placeholder/100/100" alt="QR Code" class="w-24 h-24 shadow-md rounded">
            </div>

            <div class="mt-4 text-center text-sm text-green-700">
                <p class="font-semibold">ID CARD : 202400123</p>
                <p class="font-semibold">SAISON 2024 - 2025</p>
            </div>
        </div>
    </div>
    <button onclick="generateHighQualityPDF()" class="mt-6 bg-green-600 hover:bg-green-700 text-white font-bold py-3 px-6 rounded-lg shadow-lg transition duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-105 w-full">
        Générer PDF Haute Qualité
    </button>
</div>

<script>
    function generateHighQualityPDF() {
        const badge = document.getElementById('badge');
        const scale = 2; // Augmente la résolution

        html2canvas(badge, {
            scale: scale,
            useCORS: true,
            logging: false,
            allowTaint: true
        }).then(canvas => {
            const imgData = canvas.toDataURL('image/png');
            const { jsPDF } = window.jspdf;

            // Ajuste la taille du PDF pour correspondre au ratio d'aspect du badge
            const badgeAspectRatio = badge.offsetHeight / badge.offsetWidth;
            const pdfWidth = 74; // Largeur A4 en mm
            const pdfHeight = pdfWidth * badgeAspectRatio;

            const pdf = new jsPDF({
                orientation: pdfHeight > pdfWidth ? 'portrait' : 'landscape',
                unit: 'mm',
                format: [pdfWidth, pdfHeight]
            });

            pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
            pdf.save("badge_OSMIUM_HQ.pdf");
        });
    }
</script>
</body>
</html>
