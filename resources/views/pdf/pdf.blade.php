<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carte de Membre OSMIUM</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap');
    </style>
</head>
<body class="bg-gray-100 flex justify-center items-center h-screen font-['Roboto']">
<div class="w-64 h-96 bg-white rounded-lg shadow-lg overflow-hidden relative">
    <!-- Header -->
    <div class="h-24 bg-gradient-to-r from-yellow-400 via-yellow-300 to-yellow-200 relative">
        <div class="absolute top-2 left-2 flex items-center">
            <div class="w-16 h-16 bg-white rounded-full flex items-center justify-center mr-2 border-2 border-green-700">
                <img src="https://www.claudeusercontent.com/api/placeholder/48/48" alt="Logo" class="object-cover h-full w-full rounded-full">
            </div>
            {{--https://www.claudeusercontent.com/api/placeholder/112/112--}}
            <div class="text-green-700">
                <h1 class="text-xl font-bold">OSMIUM</h1>
                <p class="text-xs">FOOTBALL CLUB YAOUNDÉ II</p>
            </div>
        </div>
    </div>

    <!-- Main Content -->
    <div class="p-4" >
        <div class="flex justify-center -mt-12">
            <div class="w-28 h-28 rounded-full overflow-hidden border-4 border-white shadow-lg">
                <img src="https://www.claudeusercontent.com/api/placeholder/112/112" alt="Member" class="w-full h-full object-cover">
            </div>
        </div>

        <div class="text-center mt-2">
            <h2 class="text-lg font-bold text-green-700">NGONO Christelle</h2>
            <p class="text-sm text-green-700 font-semibold">CARTE DE MEMBRE</p>
        </div>

        <div class="mt-4 flex justify-center">
            <img src="https://www.claudeusercontent.com/api/placeholder/80/80" alt="QR Code" class="w-20 h-20">
        </div>

        <div class="mt-2 text-center text-xs text-green-700">
            <p>ID CARD : 202400123</p>
            <p>SAISON 2024 - 2025</p>
        </div>
    </div>
</div>
</body>
</html>
