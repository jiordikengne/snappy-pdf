<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $title ??'pdf generator' }}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <style>
        @page {
            margin: 0;
            size: portrait;
        }

        body {
            font-family: 'Montserrat', sans-serif;
            background-color: #F3F4F6;
        }
    </style>
    <link rel="stylesheet" href="{!! asset('build/assets/app-CEM17nhB.css') !!}">
</head>

<body class="flex rounded-lg justify-center items-center h-full">
<div class="w-full h-full rounded-lg shadow-lg overflow-hidden">
    <!-- Header -->
    <div class="flex items-center p-4 w-full" style="background-color: red; color: white">
        <div class="w-16 h-16 bg-white rounded-full flex items-center justify-center mr-4">
            <img src="{!! asset('images/2.png') !!}" alt="Logo" class="w-12 h-12">
        </div>
        <div class="text-white">
            <h1 class="text-2xl font-bold">PNEO</h1>
            <p class="text-sm">Mentalists Agence Créative</p>
        </div>
    </div>

    <!-- Main Content -->
    <div class="p-4">
        <div class="flex justify-center -mt-16">
            <div class="w-32 h-32 rounded-full overflow-hidden shadow-lg"
                 style="border: 4px solid white ;box-shadow: 0 0 #0000">
                <img
                    src="https://images.unsplash.com/photo-1550482781-48d477e61c72?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTR8fHFyJTIwY29kZXxlbnwwfHwwfHx8MA%3D%3D"
                    alt="Member" class="w-full h-full object-cover rounded-full">
            </div>
        </div>

        <div class="text-center mt-2">
            <h2 class="text-xl font-semibold">BEN</h2>
            <p class="text-gray-600 text-sm" style="color: #0a0909">CARTE DE PRESENCE</p>
        </div>

        <div class="mt-4 flex justify-center items-center pl-28"
             style="display: flex; justify-content: center;">
            {{--            <div class="w-24 h-24" style="display: flex; justify-content: center;background-color: #313133"></div>--}}
            <img class="w-24 h-24 flex justify-center items-center"
                 src="https://images.unsplash.com/photo-1550482781-48d477e61c72?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTR8fHFyJTIwY29kZXxlbnwwfHwwfHx8MA%3D%3D">
        </div>

        <div class="mt-4 text-center text-sm">
            <p>ID CARD : 202400123</p>
            <p>Développeur Fullstack-JS</p>
        </div>
    </div>
</div>
</body>
</html>
