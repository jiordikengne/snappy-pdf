<?php

namespace App\Http\Controllers;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function generatePDF()
    {
        $data = [
            'title' => 'Mon PDF avec Laravel 11 et Snappy',
            'content' => 'Contenu généré sur Windows 11.'
        ];

        $pdf = SnappyPdf::loadView('pdf.template', $data);
        $pdf->setOption('page-size', 'A7');
        $pdf->setOption('orientation', 'Portrait');
        $pdf->setOption('margin-top', 0);
        $pdf->setOption('margin-right', 0);
        $pdf->setOption('margin-bottom', 0);
        $pdf->setOption('margin-left', 0);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('enable-local-file-access', true);
        $pdf->setOption('encoding', 'UTF-8');
        $pdf->setOption('images', true);
        $pdf->setOption('enable-external-links', true);
//        $pdf->setOption('disable-smart-shrinking', true);
        return $pdf->inline('test.pdf');
    }
}
